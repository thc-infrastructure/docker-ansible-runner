FROM python:alpine

RUN apk add --update make gcc musl-dev python3-dev libffi-dev openssl-dev cargo linux-headers musl-dev libffi-dev rsync openssh-client bind-tools bash zip gzip tar \
    && pip install --upgrade pip \
    && pip install --upgrade ansible botocore boto3 netaddr \
    && apk del make gcc linux-headers musl-dev python3-dev

WORKDIR /opt/ansible

ADD . .

# https://gitlab.com/gitlab-org/gitlab-runner/issues/2109#note_47480476
ENTRYPOINT ["/bin/bash", "-l", "-c"]
